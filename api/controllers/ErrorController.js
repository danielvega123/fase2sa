/**
 * ErrorController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {


  /**
   * undefined
   * Se llama cuando no existe la ruta de la petición
   */
  undefined: function (req, res) {
    return ResponseService.res_err(
      res, MessageTableService.get_error("", "method_unvailable")
    );
  }
};


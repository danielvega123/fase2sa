module.exports = {
  /**
   * create   
   * 
   * retorno:
   * Crea un nuevo votante
   */
  create: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let dpi = req.param('dpi'),
      sexo = req.param('sexo'),
      fechaNacimiento = req.param('fechaNacimiento'),
      municipio = req.param('municipio');
    //de no estar correcto, retornar un error
    if (!dpi) return ResponseService.res_err(
      res, MessageTableService.get_error("dpi", "mandatory_param")
    );
    if (!sexo) return ResponseService.res_err(
      res, MessageTableService.get_error("sexo", "mandatory_param")
    );
    if (!fechaNacimiento) return ResponseService.res_err(
      res, MessageTableService.get_error("fechaNacimiento", "mandatory_param")
    );
    if (!municipio) return ResponseService.res_err(
      res, MessageTableService.get_error("municipio", "mandatory_param")
    );
    //Asignar el modelo de datos
    var data_info = require('../models-ws/VotanteModel')
      .create(dpi, sexo, fechaNacimiento, municipio);
    //Instanciar la funcion
    var result = await VotanteService.create(data_info);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },

  /**
   * findOne
   * @dpi: CUI de un ciudadano
   * 
   * retorno:
   * Información sobre el votante
   */
  findOne: async function (req, res) {
    let dpi = req.param('dpi');
    //Instanciar la funcion
    var result = await VotanteService.findOne('dpi', dpi);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
  /**
   * create   
   * 
   * retorno:
   * Crea un nuevo votante
   */
  updateOne: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let dpi = req.param('dpi'),
      sexo = req.param('sexo'),
      fechaNacimiento = req.param('fechaNacimiento'),
      municipio = req.param('municipio');
    //de no estar correcto, retornar un error
    if (!dpi) return ResponseService.res_err(
      res, MessageTableService.get_error("dpi", "mandatory_param")
    );
    if (!sexo) return ResponseService.res_err(
      res, MessageTableService.get_error("sexo", "mandatory_param")
    );
    if (!fechaNacimiento) return ResponseService.res_err(
      res, MessageTableService.get_error("fechaNacimiento", "mandatory_param")
    );
    if (!municipio) return ResponseService.res_err(
      res, MessageTableService.get_error("municipio", "mandatory_param")
    );
    //Asignar el modelo de datos
    var data_info = require('../models-ws/VotanteModel')
      .create(dpi, sexo, fechaNacimiento, municipio);
    //Instanciar la funcion
    var result = await VotanteService.update(data_info);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
  /**
   * deleteOne
   * @dpi: dpi del votante a eliminar
   * 
   * retorno:
   * Elimina un votante
   */
  deleteOne: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let dpi = req.param('dpi');
    //Asignar el modelo de datos
    var data_info = require('../models-ws/VotanteModel')
      .create(dpi);
    //Instanciar la funcion
    var result = await VotanteService.delete(data_info);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
};
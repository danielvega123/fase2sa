module.exports = {
  /**
   * set_mesa
   * @dpi
   * @noMesa
   */
  set_mesa: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let dpi = req.param('dpi'),
      noMesa = req.param('noMesa');
    if (!noMesa) return ResponseService.res_err(
      res, MessageTableService.get_error("noMesa", "mandatory_param")
    );
    //Asignar el modelo de datos
    var data_info = require('../models-ws/MesaVotanteModel')
      .create(dpi, noMesa);
    //Instanciar la funcion
    var result = await MesaVotanteService.create(data_info);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
  /**
   * votar
   * @dpi
   * @id
   */
  votar: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let dpi = req.param('dpi'),
      partido = req.param('partido') || req.param('id_partido');
    //de no estar correcto, retornar un error
    if (!dpi) return ResponseService.res_err(
      res, MessageTableService.get_error("dpi", "mandatory_param")
    );
    if (!partido) return ResponseService.res_err(
      res, MessageTableService.get_error("partido", "mandatory_param")
    );
    //Asignar el modelo de datos
    var data_info = require('../models-ws/VotoModel')
      .create(dpi, partido);
    //Instanciar la funcion
    var result = await VotoService.create(data_info);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
  /**
   * get_mesa
   * @dpi
   */
  get_mesa: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let dpi = req.param('dpi');
    //Instanciar la funcion
    var result = await MesaVotanteService.findOneDpi(dpi);
    //retornar los parametros e información    
    return ResponseService.res(res, result.data, result.msg, result.error);
  },

  /**
   * check_voto
   * @dpi
   */
  check_voto: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let dpi = req.param('dpi');
    //Instanciar la funcion
    var result = await VotoService.findOne('dpi', dpi);
    //retornar los parametros e información
    return ResponseService.res(res, {}, result.msg, result.error);
  },
  /**
   * check_voto
   * @dpi
   */
  count: async function (req, res) {
    //Instanciar la funcion
    var result = await VotoService.findAll();
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },

  /**
   * check_voto
   * @dpi
   */
  count_deg: async function (req, res) {
    //Instanciar la funcion
    var result = await VotoService.findAllDEG();
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
  /**
   * check_voto
   * @dpi
   */
  count_dm: async function (req, res) {
    //Instanciar la funcion
    var result = await VotoService.findAllDM();
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },

  duplicados: async function (req, res) {
    //Instanciar la funcion
    var result = await VotoService.findAllDuplicados();
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
};
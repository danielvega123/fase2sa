module.exports = {

    votantes: async function (req, res) {
        //Verificar que el json se haya recibido correctamente
        let data_info = req.param('data')
        if (!data_info) return ResponseService.res_err(
            res, MessageTableService.get_error("data", "mandatory_param")
        );
        //Instanciar la funcion
        var result = await CargaService.load_votantes(data_info);
        //retornar los parametros e información
        return ResponseService.res(res, result.data, result.msg, result.error);
    },

    mesas: async function (req, res) {
        //Verificar que el json se haya recibido correctamente
        let data_info = req.param('data')
        if (!data_info) return ResponseService.res_err(
            res, MessageTableService.get_error("data", "mandatory_param")
        );
        //Instanciar la funcion
        var result = await CargaService.load_mesas(data_info);
        //retornar los parametros e información
        return ResponseService.res(res, result.data, result.msg, result.error);
    },

    votos: async function (req, res) {
        //Verificar que el json se haya recibido correctamente
        let data_info = req.param('data')
        if (!data_info) return ResponseService.res_err(
            res, MessageTableService.get_error("data", "mandatory_param")
        );
        //Instanciar la funcion
        var result = await CargaService.load_votos(data_info);
        //retornar los parametros e información
        return ResponseService.res(res, result.data, result.msg, result.error);
    },

    localidades: async function (req, res) {
        let departamentos = req.param('departamentos'),
            municipios = req.param('municipios');
        var departamento = null, municipio = null, departamentos_list=[];
        if (!departamentos) return ResponseService.res_err(
            res, MessageTableService.get_error("departamentos", "mandatory_param")
        );
        var departamentos_info = departamentos.split('\n');
        var municipios_info = municipios.split('\n');
        //borrar actual
        await DatabaseService.deleteCollection('localidades');
        await DatabaseService.deleteCollection('departamentos');
        for (let i = 1; i < departamentos_info.length; i++) {
            departamento = departamentos_info[i].split(';');
            departamento = { grupo_id: Number(departamento[0]), departamento_id: Number(departamento[1]), nombre: departamento[2] }
            departamentos_list.push(departamento);            
            await DatabaseService.insert('departamentos', departamento);
        }
        for (let i = 1; i < municipios_info.length; i++) {
            municipio = municipios_info[i].split(';');
            municipio = { 
                grupo_id: Number(municipio[0]),
                departamento_id: Number(municipio[1]),
                departamento: departamentos_list[municipio[1]-1].nombre, 
                municipio_id: Number(municipio[2]),
                nombre: municipio[3],
                poblacion: municipio[4],
                poblacion_acum: municipio[5]
            }
            await DatabaseService.insert('localidades', municipio);
        }        
        //retornar los parametros e información
        return ResponseService.res(res);
    }
};
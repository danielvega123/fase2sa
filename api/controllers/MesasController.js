module.exports = {
  /**
     * create
     * @DPI: DPI de la persona que consulta la mesa de votación
     * 
     * retorno:
     * Crea una nueva mesa de votacion
     */
  create: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let noMesa = req.param('noMesa'),
      rango_inicial = req.param('rango_inicial'),
      rango_final = req.param('rango_final'),
      departamento = req.param('departamento'),
      municipio = req.param('municipio');
    //de no estar correcto, retornar un error
    if (!noMesa) return ResponseService.res_err(
      res, MessageTableService.get_error("noMesa", "mandatory_param")
    );
    if (!rango_inicial) return ResponseService.res_err(
      res, MessageTableService.get_error("rango_inicial", "mandatory_param")
    );
    if (!rango_final) return ResponseService.res_err(
      res, MessageTableService.get_error("rango_final", "mandatory_param")
    );
    if (!departamento) return ResponseService.res_err(
      res, MessageTableService.get_error("departamento", "mandatory_param")
    );
    if (!municipio) return ResponseService.res_err(
      res, MessageTableService.get_error("municipio", "mandatory_param")
    );
    //Asignar el modelo de datos
    var data_info = require('../models-ws/MesaModel')
      .create(noMesa, rango_inicial, rango_final, departamento, municipio);
    //Instanciar la funcion
    var result = await MesaService.create(data_info);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
  /**
   * findOne
   * @noMesa: Id de la mesa a buscar
   * 
   * retorno:
   * Información sobre la mesa de votación
   */
  findOne: async function (req, res) {
    let noMesa = req.param('noMesa');
    //Instanciar la funcion
    var result = await MesaService.findOne('noMesa', noMesa);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
  /**
   * updateOne
   * @DPI: DPI de la persona que consulta la mesa de votación
   * 
   * retorno:
   * Actualiza una nueva mesa de votacion
   */
  updateOne: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    //Verificar que el json se haya recibido correctamente
    let noMesa = req.param('noMesa'),
      rango_inicial = req.param('rango_inicial'),
      rango_final = req.param('rango_final'),
      departamento = req.param('departamento'),
      municipio = req.param('municipio');
    //de no estar correcto, retornar un error
    if (!noMesa) return ResponseService.res_err(
      res, MessageTableService.get_error("noMesa", "mandatory_param")
    );
    if (!rango_inicial) return ResponseService.res_err(
      res, MessageTableService.get_error("rango_inicial", "mandatory_param")
    );
    if (!rango_final) return ResponseService.res_err(
      res, MessageTableService.get_error("rango_final", "mandatory_param")
    );
    if (!departamento) return ResponseService.res_err(
      res, MessageTableService.get_error("departamento", "mandatory_param")
    );
    if (!municipio) return ResponseService.res_err(
      res, MessageTableService.get_error("municipio", "mandatory_param")
    );    
    //Asignar el modelo de datos
    var data_info = require('../models-ws/MesaModel')
      .create(noMesa, rango_inicial, rango_final, departamento, municipio);    
    //Instanciar la funcion
    var result = await MesaService.update(data_info);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
  /**
   * deleteOne
   * @noMesa: noMesa de la mesa a eliminar
   * 
   * retorno:
   * Elimina una mesa de votacion
   */
  deleteOne: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let noMesa = req.param('noMesa');
    //Asignar el modelo de datos
    var data_info = require('../models-ws/MesaModel')
      .create(noMesa);
    //Instanciar la funcion
    var result = await MesaService.delete(data_info);
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
  /**
   * findOneDPI
   * @DPI: DPI de la persona que consulta la mesa de votación
   * 
   * retorno:
   * Información sobre la mesa de votación
   */
  findOneDPI: async function (req, res) {
    //Verificar que el json se haya recibido correctamente
    let dpi = req.param('dpi');
    //de no estar correcto, retornar un error
    if (!dpi) return ResponseService.res_err(
      res, MessageTableService.get_error("dpi", "mandatory_param")
    );
    //Instanciar la funcion
    var result = await MesaVotanteService.findOneDpi(dpi, 'consulta_ok');
    //retornar los parametros e información
    return ResponseService.res(res, result.data, result.msg, result.error);
  },
};
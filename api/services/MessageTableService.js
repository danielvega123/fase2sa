/**
 * get_error
 * @param {*} token 
 * @param {*} tag 
 * 
 * Retorna un mensaje de error especifico por medio de una etiqueta
 */
module.exports.get_error = function (token = "", tag = "") {
  switch (tag) {
    case "mandatory_param":
      return "El paremetro " + token + " es obligatorio";
    case "method_unvailable":
      return "Metodo no disponible";
    default:
      return token;
  }
};

module.exports.get_message = function (tag = "", data = null) {
  switch (tag) {
    //carga
    case "load_ok":
      return "Se cargaron "+data.complete+" de "+data.total+" registros";
    //general
    case "voto_emited_ok":
      return "Voto exitoso";
    case "mesa_set_ok":
      return "Ciudadano " + data.dpi + " asignado a mesa " + data.noMesa;
    case "voto_check_ok":
      return "El ciudadano " + data + " ha votado";
    case "voto_count_ok":
      return "Se contaron " + data + " cantidad de votos";
    case "consulta_ok":
      return 'Le toca votar en el departamento de '+data.departamento+', municipio: '+data.municipio+' Mixco, mesa no.: '+data.noMesa;
    case "consulta_padron_ok":
      return 'El ciudadano está empadronado';
    //negados
    case "voto_emited_not_ok":
      return "Voto no exitoso";
    case "voto_check_not_ok":
      return "El ciudadano " + data + " no ha votado";
    case "mesa_set_not_ok":
      return "Ciudadano " + data.dpi + " no asignado a mesa " + data.noMesa;
    case "consulta_not_ok":
      return "Consulta no exitosa";
    case "voto_count_not_ok":
      return "No se contaron votos";
    //mesas
    case "mesa_create_ok":
      return "Mesa registrada";
    case "mesa_update_ok":
      return "Mesa modificada";
    case "mesa_delete_ok":
      return "Mesa eliminada exitosamente";
    case "info_mesa_ok":
      return "La mesa existe";
    //mesas negadas
    case "mesa_create_not_ok":
      return "Mesa no registrada";
    case "mesa_update_not_ok":
      return "Mesa no modificada";
    case "mesa_delete_not_ok":
      return "Mesa no eliminada";
    case "info_mesa_not_ok":
      return "Información de mesa no encontrada";
    //votantes
    case "votante_create_ok":
      return "Votante registrado";
    case "info_votante_ok":
      return "Información del votante";
    case "votante_update_ok":
      return "Datos del votante actualizados";
    case "votante_delete_ok":
      return "Registro del votante eliminado exitosamente";
    //votantes negados
    case "votante_create_not_ok":
      return "Votante no registrado";
    case "info_votante_not_ok":
      return "Información del votante no encontrada";
    case "votante_update_not_ok":
      return "Datos del votante no actualizados";
    case "votante_delete_not_ok":
      return "Registro del votante no eliminado exitosamente";
    default:
      return tag;
  }
};
const grupo = 1;
module.exports.load_votantes = async function (data = null) {
    var errores = [];
    var result = {
        total: data.length,
        complete: 0
    }
    var error = true;
    for (let i = 0; i < data.length; i++) {
        var localidad = await this.findOne('municipio_id', data[i].municipio);
        if (localidad.error) errores.push('No existe un municipio con código: ' + data[i].municipio)
        else if (localidad.data.grupo_id !== grupo) errores.push('Pertenece al grupo: ' + localidad.data.grupo_id)
        else {
            res = await VotanteService.create(data[i]);
            if (!res.error) result.complete++;
            else errores.push('La persona con dpi: ' + data[i].dpi + ' ya esta registrada');
        }
    }
    msg = MessageTableService.get_message('load_ok', result);
    var res = {
        errores: errores
    };
    if (!errores.length) error = false;
    return ResponseService.return_result(msg, error, res);
}

module.exports.load_mesas = async function (data = null) {
    var errores = [];
    var result = {
        total: data.length,
        complete: 0
    }
    var error = true;
    for (let i = 0; i < data.length; i++) {
        var localidad = await this.findOne('municipio_id', data[i].municipio);
        if (localidad.error) errores.push('No existe un municipio con código: ' + data[i].municipio)
        //else if (localidad.data.grupo_id !== grupo) errores.push('Pertenece al grupo: ' + localidad.data.grupo_id)
        else {
            res = await MesaService.create(data[i]);
            if (!res.error) result.complete++;
            else errores.push('La mesa con número: ' + data[i].noMesa + ' ya esta registrada');
        }
    }
    msg = MessageTableService.get_message('load_ok', result);
    var res = {
        errores: errores
    };
    if (!errores.length) error = false;
    return ResponseService.return_result(msg, error, res);
}

module.exports.load_votos = async function (data = null) {
    var errores = [];
    var result = {
        total: data.length,
        complete: 0
    }
    var error = true;
    for (let i = 0; i < data.length; i++) {
        data[i].dpi=Number(data[i].dpi);
        res = await VotoService.create(data[i]);
        if (!res.error) result.complete++;
        else errores.push(res.msg+ ' dpi: '+data[i].dpi+ ' partido: '+data[i].partido);
    }
    msg = MessageTableService.get_message('load_ok', result);
    var res = {
        errores: errores
    };
    if (!errores.length) error = false;
    return ResponseService.return_result(msg, error, res);
}

module.exports.findOne = async function (field, value) {
    var data = null;
    var error = true, msg = '';
    await DatabaseService.findOne('localidades', field, value).then(_result => {
        if (_result && _result.length) {
            data = JSON.parse(_result)[0] || null;
            if (data) error = false;
        }
    });
    return ResponseService.return_result(msg, error, data);
}

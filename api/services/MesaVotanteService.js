module.exports.create = async function (data = null) {
    var msg = MessageTableService.get_message('mesa_set_not_ok', data);
    var error = true;
    //search exists
    var mesa_votante = await this.findOne('dpi', data.dpi);
    if (mesa_votante.data) return ResponseService.return_result(msg, error);
    //search mesa
    var mesa = await MesaService.findOne('noMesa', data.noMesa);
    //search votante
    var votante = await VotanteService.findOne('dpi', data.dpi);
    if (!mesa.data || !votante.data) return ResponseService.return_result(msg, error);
    await DatabaseService.insert('mesas_votantes', data).then(_result => {
        if (_result) {
            msg = MessageTableService.get_message('mesa_set_ok', data);
            error = false;
        }
    });
    return ResponseService.return_result(msg, error);
}
module.exports.findOne = async function (field, value) {
    var data = null;
    var msg = MessageTableService.get_message('info_votante_not_ok');
    var error = true;
    await DatabaseService.findOne('mesas_votantes', field, value).then(_result => {
        if (!_result && !_result.length) msg = MessageTableService.get_message('info_votante_not_ok');
        else {
            msg = MessageTableService.get_message('info_votante_ok');
            data = JSON.parse(_result)[0] || null;
            if (data) error = false;
        }
    });
    return ResponseService.return_result(msg, error, data);
}
module.exports.findOneDpi = async function (dpi, text='consulta_padron_ok') {
    var msg = MessageTableService.get_message('consulta_not_ok');
    var error = true;
    //search empadronamiento
    var mesa_votante = await MesaVotanteService.findOne('dpi', dpi);
    if (!mesa_votante.data) return ResponseService.return_result(msg, error);
    //search mesa
    var mesa = await MesaService.findOne('noMesa', mesa_votante.data.noMesa);
    if (!mesa.data) return ResponseService.return_result(msg, error);
    error = false;
    //return data
    msg = MessageTableService.get_message(text,mesa.data);
    return ResponseService.return_result(msg, error, mesa.data);
}

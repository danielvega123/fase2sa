/**
 * res
 * @param {*} res 
 * @param {*} data 
 * @param {*} msg 
 * @param {*} error 
 * 
 * Retorna la petición
 */
module.exports.res = function (res, data={}, msg = "", error = false) {
    delete data.id;
    res.ok({
        hayError: error,
        mensaje: msg,
        objRespuesta: data,
    });
}

/**
 * res_err
 * @param {*} res 
 * @param {*} data 
 * @param {*} msg 
 * @param {*} error 
 * Retorna la petición con errores
 */
module.exports.res_err = function (res, msg = "", data = {}) {
    res.ok({
        hayError: true,
        mensaje: msg,
        objetoRespuesta: data,
    });
}

module.exports.return_result = function (msg, error, data = {}) {
    return {
        error: error,
        data: data,
        msg: msg
    };
}
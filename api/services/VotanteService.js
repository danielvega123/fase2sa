module.exports.create = async function (data = null) {
    var msg = MessageTableService.get_message('votante_create_not_ok');
    var error = true;
    //search votante    
    var votante = await this.findOne('dpi', data.dpi);
    if (votante.data) return ResponseService.return_result(msg, error);
    await DatabaseService.insert('votantes', data).then(_result => {
        if (_result) {
            msg = MessageTableService.get_message('votante_create_ok');
            error = false;
        }
    });
    return ResponseService.return_result(msg, error);
}
module.exports.findOne = async function (field, value) {
    var data = null;
    var msg = MessageTableService.get_message('info_votante_not_ok');
    var error = true;
    await DatabaseService.findOne('votantes', field, value).then(_result => {
        if (_result!==null && !_result && !_result.length) msg = MessageTableService.get_message('info_votante_not_ok');
        else {
            msg = MessageTableService.get_message('info_votante_ok');
            data = JSON.parse(_result)[0] || null;
            if (data) error = false;
        }
    });
    return ResponseService.return_result(msg, error, data);
}
module.exports.update = async function (data = null) {
    var msg = MessageTableService.get_message('votante_update_not_ok');
    var error = true;
    //search votante    
    var votante = await this.findOne('dpi', data.dpi);
    if (!votante.data) return ResponseService.return_result(msg, error);
    await DatabaseService.update('votantes', votante, data).then(_result => {
        if (_result) {
            msg = MessageTableService.get_message('votante_update_ok');
            error = false;
        }
    });
    return ResponseService.return_result(msg, error);
}
module.exports.delete = async function (data = null) {
    var msg = MessageTableService.get_message('votante_delete_not_ok');
    var error = true;
    //search votante
    var votante = await this.findOne('dpi', data.dpi);
    if (!votante.data) return ResponseService.return_result(msg, error);
    await DatabaseService.delete('votantes', votante, data).then(_result => {
        msg = MessageTableService.get_message('votante_delete_ok');
        error = false;
    });
    return ResponseService.return_result(msg, error);
}

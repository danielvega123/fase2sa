module.exports.create = async function (data = null) {
    var msg = MessageTableService.get_message('voto_emited_not_ok');
    var error = true, duplicado = null;
    //search exists
    var voto = await this.findOne('dpi', data.dpi);
    if (voto.data) {
        try {
            duplicado = JSON.parse(await DatabaseService.findOne('duplicados', 'dpi', data.dpi))[0];
        } catch (error) {
            duplicado = null;
        }
        if (!duplicado) {
            await DatabaseService.insert('duplicados', { dpi: data.dpi, total: 1 });
        } else {
            duplicado.total++;
            var da={ data:{ id:duplicado.id } };
            await DatabaseService.update('duplicados', da, duplicado);
        }
        return ResponseService.return_result(msg, error);
    }
    //search votante
    var votante = await VotanteService.findOne('dpi', data.dpi);
    if (!votante.data) return ResponseService.return_result(msg, error);
    await DatabaseService.insert('votos', data).then(_result => {
        if (_result) {
            msg = MessageTableService.get_message('voto_emited_ok', data, data);
            error = false;
        }
    });
    return ResponseService.return_result(msg, error);
}
module.exports.findOne = async function (field, value) {
    var data = null;
    var msg = MessageTableService.get_message('voto_check_not_ok', value);
    var error = true;
    await DatabaseService.findOne('votos', field, value).then(_result => {
        if (!_result && !_result.length) msg = MessageTableService.get_message('info_votante_not_ok');
        else {
            data = JSON.parse(_result)[0] || null;
            if (data) {
                error = false;
                msg = MessageTableService.get_message('voto_check_ok', value);
            }
        }
    });
    return ResponseService.return_result(msg, error, data);
}
module.exports.findAll = async function () {
    var data = null;
    var msg = MessageTableService.get_message('voto_count_not_ok');
    var error = true;
    var p1 = 0, p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0, p7 = 0, nulo = 0;
    await DatabaseService.findAll('votos').then(_result => {
        if (!_result && !_result.length) msg = MessageTableService.get_message('info_votante_not_ok');
        else {
            data = JSON.parse(_result) || null;
            if (data) {
                error = false;
                msg = MessageTableService.get_message('voto_count_ok', data.length);
                for (let i = 0; i < data.length; i++) {
                    switch (data[i].partido) {
                        case 1:
                            p1++;
                            break;
                        case 2:
                            p2++;
                            break;
                        case 3:
                            p3++;
                            break;
                        case 4:
                            p4++;
                            break;
                        case 5:
                            p5++;
                            break;
                        case 6:
                            p6++;
                            break;
                        case 7:
                            p7++;
                            break;
                        default:
                            p6++;
                    }
                }
            }
        }
    });
    data = {
        votos: [
            { idPartido: 1, nombrePartido: "Partido 1", candidato: "Candidato 1", votos: p1 },
            { idPartido: 2, nombrePartido: "Partido 2", candidato: "Candidato 2", votos: p2 },
            { idPartido: 3, nombrePartido: "Partido 3", candidato: "Candidato 3", votos: p3 },
            { idPartido: 4, nombrePartido: "Partido 4", candidato: "Candidato 4", votos: p4 },
            { idPartido: 5, nombrePartido: "Partido 5", candidato: "Candidato 5", votos: p5 },
            { idPartido: 6, nombrePartido: "Nulo", candidato: "Nulo", votos: p6 },
            { idPartido: 7, nombrePartido: "Blanco", candidato: "Blanco", votos: p7 }
        ]
    };
    return ResponseService.return_result(msg, error, data);
}

module.exports.findAllDEG = async function () {
    var data = null;
    var msg = MessageTableService.get_message('voto_count_not_ok');
    var error = true;
    var departamento = null, partido = null, votante = null, localidad = null, index = 0, edad = 0;
    var partidos = [
        { idPartido: 1, nombrePartido: "Partido 1", candidato: "Candidato 1", votos: [], votosDepto: [] },
        { idPartido: 2, nombrePartido: "Partido 2", candidato: "Candidato 2", votos: [], votosDepto: [] },
        { idPartido: 3, nombrePartido: "Partido 3", candidato: "Candidato 3", votos: [], votosDepto: [] },
        { idPartido: 4, nombrePartido: "Partido 4", candidato: "Candidato 4", votos: [], votosDepto: [] },
        { idPartido: 5, nombrePartido: "Partido 5", candidato: "Candidato 5", votos: [], votosDepto: [] },
        { idPartido: 6, nombrePartido: "Nulo", candidato: "Nulo", votos: [], votosDepto: [] },
        { idPartido: 7, nombrePartido: "Blanco", candidato: "Blanco", votos: [], votosDepto: [] }
    ];
    var departamentos = JSON.parse(await DatabaseService.findAll('departamentos'));
    await DatabaseService.findAll('votos').then(_result => {
        if (!_result && !_result.length) msg = MessageTableService.get_message('info_votante_not_ok');
        else {
            data = JSON.parse(_result) || null;
            if (data) {
                error = false;
                msg = MessageTableService.get_message('voto_count_ok', data.length);
                for (let i = 0; i < data.length; i++) {
                    switch (data[i].partido) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            partidos[data[i].partido - 1].votos.push(data[i]);
                            break;
                        default:
                            partidos[6].votos.push(data[i]);
                    }
                }
            }
        }
    });
    for (let i = 0; i < partidos.length; i++) {
        partido = partidos[i];
        for (let j = 0; j < departamentos.length; j++) {
            departamento = {
                idDepto: departamentos[j].departamento_id,
                nombre: departamentos[j].nombre,
                votos: 0,
                votosM: 0,
                votosF: 0,
                M18_25: 0,
                F18_25: 0,
                M26_35: 0,
                F26_35: 0,
                M36_45: 0,
                F36_45: 0,
                M46_55: 0,
                F46_55: 0,
                M56_00: 0,
                F56_00: 0,
            };
            partidos[i].votosDepto.push(departamento);
        }
    }
    for (let i = 0; i < partidos.length; i++) {
        partido = partidos[i];
        for (let j = 0; j < partido.votos.length; j++) {
            data = partido.votos[j];
            votante = (await VotanteService.findOne('dpi', data.dpi)).data;
            localidad = JSON.parse(await DatabaseService.findOne('localidades', 'municipio_id', votante.municipio))[0];
            switch (data.partido) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    index = data.partido - 1;
                    break;
                default:
                    index = 6;
            }
            partidos[index].votosDepto[localidad.departamento_id - 1].votos++;
            edad = votante.fechaNacimiento.toString().substring(0, 4);
            edad = 2018 - Number(edad);
            if (votante.sexo === 'F') {
                partidos[index].votosDepto[localidad.departamento_id - 1].votosF++;
                if (edad < 26)
                    partidos[index].votosDepto[localidad.departamento_id - 1].F18_25++;
                else if (edad < 36)
                    partidos[index].votosDepto[localidad.departamento_id - 1].F26_35++;
                else if (edad < 46)
                    partidos[index].votosDepto[localidad.departamento_id - 1].F36_45++;
                else if (edad < 56)
                    partidos[index].votosDepto[localidad.departamento_id - 1].F46_55++;
                else
                    partidos[index].votosDepto[localidad.departamento_id - 1].F56_00++;
            }
            else {
                partidos[index].votosDepto[localidad.departamento_id - 1].votosM++;
                if (edad < 26)
                    partidos[index].votosDepto[localidad.departamento_id - 1].M18_25++;
                else if (edad < 36)
                    partidos[index].votosDepto[localidad.departamento_id - 1].M26_35++;
                else if (edad < 46)
                    partidos[index].votosDepto[localidad.departamento_id - 1].M36_45++;
                else if (edad < 56)
                    partidos[index].votosDepto[localidad.departamento_id - 1].M46_55++;
                else
                    partidos[index].votosDepto[localidad.departamento_id - 1].M56_00++;
            }
        }
        delete partido.votos;
    }
    data = {
        votosPartido: partidos
    }
    return ResponseService.return_result(msg, error, data);
}

module.exports.findAllDM = async function () {
    var data = null;
    var msg = MessageTableService.get_message('voto_count_not_ok');
    var error = true;
    var departamento = null, partido = null, votante = null, localidad = null, index = 0, edad = 0, localidades = null;
    var partidos = [
        { idPartido: 1, nombrePartido: "Partido 1", candidato: "Candidato 1", votos: [], votosDepto: [] },
        { idPartido: 2, nombrePartido: "Partido 2", candidato: "Candidato 2", votos: [], votosDepto: [] },
        { idPartido: 3, nombrePartido: "Partido 3", candidato: "Candidato 3", votos: [], votosDepto: [] },
        { idPartido: 4, nombrePartido: "Partido 4", candidato: "Candidato 4", votos: [], votosDepto: [] },
        { idPartido: 5, nombrePartido: "Partido 5", candidato: "Candidato 5", votos: [], votosDepto: [] },
        { idPartido: 6, nombrePartido: "Nulo", candidato: "Nulo", votos: [], votosDepto: [] },
        { idPartido: 7, nombrePartido: "Blanco", candidato: "Blanco", votos: [], votosDepto: [] }
    ];
    var departamentos = JSON.parse(await DatabaseService.findAll('departamentos'));
    localidades = JSON.parse(await DatabaseService.findAll('localidades'));
    await DatabaseService.findAll('votos').then(async _result => {
        if (!_result && !_result.length) msg = MessageTableService.get_message('info_votante_not_ok');
        else {
            data = JSON.parse(_result) || null;
            if (data) {
                error = false;
                msg = MessageTableService.get_message('voto_count_ok', data.length);
                for (let i = 0; i < data.length; i++) {
                    switch (data[i].partido) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            partidos[data[i].partido - 1].votos.push(data[i]);
                            break;
                        default:
                            partidos[6].votos.push(data[i]);
                    }
                    votante = (await VotanteService.findOne('dpi', data[i].dpi)).data;
                    localidad = JSON.parse(await DatabaseService.findOne('localidades', 'municipio_id', votante.municipio))[0];
                    localidades[localidad.municipio_id - 1].votos = (localidades[localidad.municipio_id - 1].votos || 0) + 1;
                }
            }
        }
    });
    for (let i = 0; i < partidos.length; i++) {
        partido = partidos[i];
        for (let j = 0; j < departamentos.length; j++) {
            departamento = {
                idDepto: departamentos[j].departamento_id,
                nombre: departamentos[j].nombre,
                votos: 0,
                municipios: [],
            };
            partidos[i].votosDepto.push(departamento);
        }
        for (let j = 0; j < localidades.length; j++) {
            localidad = localidades[j];
            municipio = {
                idMunicipio: localidad.municipio_id,
                nombre: localidad.nombre,
                votos: localidad.votos || 0
            };
            partidos[i].votosDepto[localidad.departamento_id - 1].municipios.push(municipio);
        }
    }
    for (let i = 0; i < partidos.length; i++) {
        partido = partidos[i];
        for (let j = 0; j < partido.votosDepto.length; j++) {
            departamento = partido.votosDepto[j];
            for (let k = 0; k < departamento.municipios.length; k++) {
                municipio = departamento.municipios[k];
                partidos[i].votosDepto[j].votos += municipio.votos || 0;
            }
        }
        delete partido.votos;
    }
    data = {
        votos: partidos
    }
    return ResponseService.return_result(msg, error, data);
}

module.exports.findAllDuplicados = async function () {
    var msg = 'Votos duplicados';
    var error = false;
    var duplicados = JSON.parse(await DatabaseService.findAll('duplicados'));
    var data={
        duplicados:duplicados
    }
    return ResponseService.return_result(msg, error, data);
}
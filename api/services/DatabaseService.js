const request = require('request');
const api_db = 'http://35.226.60.40:3000/api/'
module.exports.insert = function (collection, data) {
    var url = api_db + collection;
    return request_db(url, "POST", data);
}
module.exports.update = function (collection, document, data) {
    var url = get_url_document_id(collection, document);
    return request_db(url, "PUT", data);
}
module.exports.delete = function (collection, document, data) {
    var url = get_url_document_id(collection, document);
    return request_db(url, "DELETE", data);
}
module.exports.findOne = function (collection, field, value) {
    var url = api_db + collection + '?' + field + '=' + value;
    return request_db(url, "GET");
}
module.exports.findAll = function (collection) {
    var url = api_db + collection;
    return request_db(url, "GET");
}
//default
module.exports.deleteCollection = function (collection) {
    var url = api_db + collection;
    return request_db(url, "DELETE");
}
function get_url_document_id(collection, document) {
    if (document.data && document.data.id)
        return api_db + collection + '/' + document.data.id;
    return api_db + collection + '/' + 0;
}

function request_db(url, method, data = null) {
    return new Promise(
        function (resolve, reject) {
            request({
                url: url,
                method: method,
                json: data
            }, function (error, response, body) {
                if (error) resolve(null);
                resolve(body);
            });
        }
    );
}
module.exports.create = async function (data = null) {
    var msg = MessageTableService.get_message('mesa_create_not_ok');
    var error = true;
    //search mesa    
    var mesa = await this.findOne('noMesa', data.noMesa);
    if (mesa.data) return ResponseService.return_result(msg, error);
    await DatabaseService.insert('mesas', data).then(_result => {
        if (_result) {
            msg = MessageTableService.get_message('mesa_create_ok');
            error = false;
        }
    });
    return ResponseService.return_result(msg, error);
}
module.exports.update = async function (data = null) {
    var msg = MessageTableService.get_message('mesa_update_not_ok');
    var error = true;
    //search mesa    
    var mesa = await this.findOne('noMesa', data.noMesa);
    if (!mesa.data) return ResponseService.return_result(msg, error);
    await DatabaseService.update('mesas', mesa, data).then(_result => {
        if (_result) {
            msg = MessageTableService.get_message('mesa_update_ok');
            error = false;
        }
    });
    return ResponseService.return_result(msg, error);
}
module.exports.delete = async function (data = null) {
    var msg = MessageTableService.get_message('mesa_delete_not_ok');
    var error = true;
    //search mesa
    var mesa = await this.findOne('noMesa', data.noMesa);
    if (!mesa.data) return ResponseService.return_result(msg, error);
    await DatabaseService.delete('mesas', mesa, data).then(_result => {
        msg = MessageTableService.get_message('mesa_delete_ok');
        error = false;
    });
    return ResponseService.return_result(msg, error);
}
module.exports.findOne = async function (field, value) {
    var data = null;
    var msg = MessageTableService.get_message('info_mesa_not_ok');
    var error = true;
    await DatabaseService.findOne('mesas', field, value).then(_result => {
        if (!_result && !_result.length) msg = MessageTableService.get_message('info_mesa_not_ok');
        else {
            msg = MessageTableService.get_message('info_mesa_ok');
            data = JSON.parse(_result)[0] || null;
            if (data) error = false;
        }
    });
    return ResponseService.return_result(msg, error, data);
}
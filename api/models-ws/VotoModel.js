module.exports.create = function (dpi, partido) {
    return {
        dpi: Number(dpi),
        partido: Number(partido),
    };
}
module.exports.create = function (dpi, noMesa) {
    return {
        dpi: Number(dpi),
        noMesa: Number(noMesa),
    };
}
module.exports.create = function (dpi, sexo = null, fechaNacimiento = null, municipio = null) {
    return {
        dpi: Number(dpi),
        sexo: sexo,
        municipio: Number(municipio),
        fechaNacimiento: fechaNacimiento
    };
}
module.exports.create = function (noMesa, rango_inicial=null, rango_final=null, departamento=null, municipio=null) {
    return {
        noMesa: Number(noMesa),
        rango_inicial: Number(rango_inicial),
        rango_final: Number(rango_final),
        departamento: Number(departamento),
        municipio: Number(municipio)
    };
}
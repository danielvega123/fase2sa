// test/integration/controllers/VotosController.test.js
var supertest = require('supertest');
describe('VotosController.count', function () {
    describe('#count()', function () {
        it('return json', function (done) {
            supertest(sails.hooks.http.app)
                .post('/ws/rest/votos')
                .expect(200, done);
        });
    });
});
describe('VotosController.check_voto', function () {
    describe('#check_voto()', function () {
        it('return json', function (done) {
            supertest(sails.hooks.http.app)
                .post('/ws/rest/votantes/votos/4434')
                .expect(200, done);
        });
    });
});
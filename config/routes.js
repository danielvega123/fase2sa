module.exports.routes = {
  //votos
  'POST /ws/rest/votos/:id_partido': 'VotosController.votar',
  'GET /ws/rest/votos/:id_partido': 'ErrorController.undefined',
  'PUT /ws/rest/votos/:id_partido': 'ErrorController.undefined',
  'DELETE /ws/rest/votos/:id_partido': 'ErrorController.undefined',
  
  'POST /ws/rest/votos': 'ErrorController.undefined',
  'GET /ws/rest/votos': 'VotosController.count',
  'PUT /ws/rest/votos': 'ErrorController.undefined',
  'DELETE /ws/rest/votos': 'ErrorController.undefined',
  
  //mesas
  'POST /ws/rest/mesas': 'MesasController.create',
  'GET /ws/rest/mesas': 'ErrorController.undefined',
  'PUT /ws/rest/mesas': 'ErrorController.undefined',
  'DELETE /ws/rest/mesas': 'ErrorController.undefined',

  //mesas
  'POST /ws/rest/mesas/consulta': 'MesasController.findOneDPI',
  'GET /ws/rest/mesas/consulta': 'MesasController.findOneDPI',
  'PUT /ws/rest/mesas/consulta': 'ErrorController.undefined',
  'DELETE /ws/rest/mesas/consulta': 'ErrorController.undefined',

  'POST /ws/rest/mesas/:noMesa': 'ErrorController.undefined',
  'GET /ws/rest/mesas/:noMesa': 'MesasController.findOne',
  'PUT /ws/rest/mesas/:noMesa': 'MesasController.updateOne',
  'DELETE /ws/rest/mesas/:noMesa': 'MesasController.deleteOne',

  //votantes
  'POST /ws/rest/votantes': 'VotantesController.create',
  'GET /ws/rest/votantes': 'ErrorController.undefined',
  'PUT /ws/rest/votantes': 'ErrorController.undefined',
  'DELETE /ws/rest/votantes': 'ErrorController.undefined',

  'POST /ws/rest/votantes/:dpi': 'ErrorController.undefined',
  'GET /ws/rest/votantes/:dpi': 'VotantesController.findOne',
  'PUT /ws/rest/votantes/:dpi': 'VotantesController.updateOne',
  'DELETE /ws/rest/votantes/:dpi': 'VotantesController.deleteOne',
  //asignar
  'POST /ws/rest/votantes/mesas/:dpi': 'VotosController.set_mesa',
  'GET /ws/rest/votantes/mesas/:dpi': 'VotosController.get_mesa',
  'PUT /ws/rest/votantes/mesas/:dpi': 'ErrorController.undefined',
  'DELETE /ws/rest/votantes/mesas/:dpi': 'ErrorController.undefined',
  //asignar
  'POST /ws/rest/votantes/votos/:dpi': 'ErrorController.undefined',
  'GET /ws/rest/votantes/votos/:dpi': 'VotosController.check_voto',
  'PUT /ws/rest/votantes/votos/:dpi': 'ErrorController.undefined',
  'DELETE /ws/rest/votantes/votos/:dpi': 'ErrorController.undefined',

  //cargas votantes
  'POST /ws/rest/carga/votantes': 'CargaController.votantes',
  'GET /ws/rest/carga/votantes': 'ErrorController.undefined',
  'PUT /ws/rest/carga/votantes': 'ErrorController.undefined',
  'DELETE /ws/rest/carga/votantes': 'ErrorController.undefined',

  //cargas mesas
  'POST /ws/rest/carga/mesas': 'CargaController.mesas',
  'GET /ws/rest/carga/mesas': 'ErrorController.undefined',
  'PUT /ws/rest/carga/mesas': 'ErrorController.undefined',
  'DELETE /ws/rest/carga/mesas': 'ErrorController.undefined',

//cargas votos
'POST /ws/rest/carga/votos': 'CargaController.votos',
'GET /ws/rest/carga/votos': 'ErrorController.undefined',
'PUT /ws/rest/carga/votos': 'ErrorController.undefined',
'DELETE /ws/rest/carga/votos': 'ErrorController.undefined',

  //localidades
  'POST /ws/rest/carga/localidades': 'CargaController.localidades',
  'GET /ws/rest/carga/localidades': 'ErrorController.undefined',
  'PUT /ws/rest/carga/localidades': 'ErrorController.undefined',
  'DELETE /ws/rest/carga/localidades': 'ErrorController.undefined',

  //agrupar
  'POST /ws/rest/votos/deg': 'ErrorController.undefined',
  'GET /ws/rest/votos/deg': 'VotosController.count_deg',
  'PUT /ws/rest/votos/deg': 'ErrorController.undefined',
  'DELETE /ws/rest/votos/deg': 'ErrorController.undefined',

  //agrupar
  'POST /ws/rest/votos/dm': 'ErrorController.undefined',
  'GET /ws/rest/votos/dm': 'VotosController.count_dm',
  'PUT /ws/rest/votos/dm': 'ErrorController.undefined',
  'DELETE /ws/rest/votos/dm': 'ErrorController.undefined',

  //agrupar
  'POST /ws/rest/votos/duplicados': 'ErrorController.undefined',
  'GET /ws/rest/votos/duplicados': 'VotosController.duplicados',  
  'PUT /ws/rest/votos/duplicados': 'ErrorController.undefined',
  'DELETE /ws/rest/votos/duplicados': 'ErrorController.undefined',
};

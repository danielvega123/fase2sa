
module.exports.models = {


  attributes: {
    createdAt: { type: 'number', autoCreatedAt: true, },
    updatedAt: { type: 'number', autoUpdatedAt: true, },
    id: { type: 'number', autoIncrement: true, },
  },

  dataEncryptionKeys: {
    default: 'qmQiuIgmBdkw6JVeQdpu0q7lCntpfz88exg4/exnXvs='
  },

  cascadeOnDestroy: true,
  migrate:'alter',


};
